FROM tomcat:9.0.14-jre8-alpine
ARG TARGET=target
RUN set -eux;\
    mkdir ${CATALINA_HOME}/webapps/email_verification_service
COPY ${TARGET}/email_verification_service/index.html ${CATALINA_HOME}/webapps/email_verification_service/
COPY ${TARGET}/email_verification_service/password-reset.html ${CATALINA_HOME}/webapps/email_verification_service/
COPY jquery/jquery-3.3.1.min.js ${CATALINA_HOME}/webapps/email_verification_service/
ENTRYPOINT ["catalina.sh"]
CMD ["run"]

# base image:
# https://hub.docker.com/_/tomcat
# https://github.com/docker-library/tomcat/blob/f58a6b4236cfe10672c9505aab5024100c9e084d/9.0/jre11-slim/Dockerfile
