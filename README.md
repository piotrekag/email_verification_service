###Docker commands

#####[Dockerhub](https://cloud.docker.com/u/piotrekag/repository/docker/piotrekag/evs) for the project

This project consists of simple landing pages for the [Spring Selfedu](https://bitbucket.org/piotrekag/spring_selfedu) 

After updating static html and js, run:

    # this creates deployable .war file
    ./mvnw clean install
    
Then:
    
    docker build .

Tag and push image:

    docker tag local-image:tagname repo:tagname
    docker push repo:tagname

E.g.:

    docker tag a83c0341bb24 piotrekag/evs:0.1.1
    docker push piotrekag/evs:0.1.1
